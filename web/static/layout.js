/**
 * DF Core Layout jQuery Functions
 */

$(function() {
    /* Auto-header navigation.
    $('div.navbar a.here').closest('li').addClass('active');

    $('div.navbar div.nav-collapse > ul').addClass('nav');
    $('ul.nav li > ul').each(function() {
        $(this).closest('li').addClass('dropdown');
        $(this).prev('a').attr('href', '#').attr('data-toggle', 'dropdown').addClass('dropdown-toggle').append(' <b class="caret"></b>');
        $(this).addClass('dropdown-menu');
    });
    */

    /* Register global AJAX handler. */
    $.ajaxSetup({ global: true });
    $(document.body).ajaxComplete(function(e) {
        initPage(e.target);
    });

    initPage($(document.body));
});

/* Page initialization function. */
function initPage(page) {

    /* Clean up display of forms. */
    $(page).find('form.df-form').each(function() {
        $(this).find('input[type="submit"],input[type="reset"],button')
            .not('.mid-form')
            .addClass('btn')
            .wrapAll('<div class="form-actions" />');

        $(this).find('button[type="submit"],input[type="submit"]')
            .addClass('btn-primary');

        $(this).find('.control-group').addClass('form-group');

        $(this).find('input:not(:checkbox):not(:radio):not(:button),textarea,select').addClass('form-control');
        $(this).find('.half-width').closest('.form-group').addClass('row').wrapInner('<div class="col-md-6" />');

        $(this).find('span.help-block').each(function() {
            $(this).insertAfter($(this).siblings('input,textarea'));
        });

        $(this).find('.checkbox-group.block label').wrap('<div class="checkbox" />');
        $(this).find('.radio-group.block label').wrap('<div class="radio" />');

        $(this).find('span.error').closest('div.clearfix').addClass('error');
    });

    /* Bootstrap 2.0 forward compatibility */
    $(page).find('.datatable').addClass('table table-bordered');
    $(page).find('form .actions').addClass('form-actions');

    $(page).find('.btn.primary,.btn.blue').addClass('btn-primary');
    $(page).find('.btn.warning,.btn.yellow').addClass('btn-warning');
    $(page).find('.btn.danger,.btn.red').addClass('btn-danger');
    $(page).find('.btn.success,btn.green').addClass('btn-success');
    $(page).find('.btn.small,.btn.btn-small').addClass('btn-sm');
    $(page).find('.btn.large,.btn.btn-large').addClass('btn-lg');

    $(page).find('.alert-message').addClass('alert');
    $(page).find('.block-message').addClass('alert-block');
    $(page).find('.alert.info,.alert.blue').addClass('alert-info');
    $(page).find('.alert.danger,.alert.red').addClass('alert-danger');
    $(page).find('.alert.success,alert.green').addClass('alert-success');

    /* Form validation. */
    if (jQuery.fn.validate)
    {
        $(page).find('form.validate').validate();
    }

    /* Tooltips */
    if (jQuery.fn.twipsy)
    {
        $(page).find('a[rel=tooltip]').twipsy({placement: 'right', html: true});
    }

    /* Pagination */
    $(page).find('div.pagination li.disabled, div.pagination li.active').click(function(e) {
        e.preventDefault();
        return false;
    });

    $(page).find('table.zebra').removeClass('zebra').addClass('table-striped');

    $(page).find('dl.zebra:odd,fieldset.zebra:odd').addClass('odd');
    $(page).find('dl.zebra:even,fieldset.zebra:even').addClass('even');

    /* Wrappers for confirmation functions. */
    $(page).find('.confirm-action,.confirm-delete,.btn.warning').click(function(e) {
        var thistitle = $(this).attr('title');

        if (thistitle)
            var message = 'Are you sure you want to '+thistitle+'?';
        else
            var message = 'Are you sure you want to complete this action?';

        if (!confirm(message))
        {
            e.preventDefault();
            return false;
        }
    });

    /* Disable submit button to prevent double submissions */
    $(page).find('form').submit(function(){
        $(this).find('input[type=submit],button[type=submit]').attr('disabled', 'disabled').addClass('disabled').val('Working...');
    });

    /* Suppress the backspace key. */
    $(page).find('select').keypress(function(event) { return cancelBackspace(event) });
    $(page).find('select').keydown(function(event) { return cancelBackspace(event) });

    /* Auto-select tabs */
    $(page).find('.autoselect').each(function() {
        var active = $(this).attr('rel');
        $(this).find('li[rel="'+active+'"]').addClass('active');
    });

    /* Form validation. */
    if (jQuery.fn.fancybox)
    {
        $(page).find('.fancybox').fancybox({
            maxWidth	: 800,
            maxHeight	: 600,
            autoSize	: true,
            fitToView	: true,
            width		: '50%',
            height		: '70%',
            arrows		: false,
            closeClick	: false,
            closeBtn	: true,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    }
}

/* Prevents the backspace key from navigating backwards on dropdown forms. */
function cancelBackspace(event) {
    if (event.keyCode == 8) {
        return false;
    }
}