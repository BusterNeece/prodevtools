<?php
return array(
    'default' => array(
        'admin' => array(
            'label' => 'Administer',
            'module' => 'admin',
            'show_only_with_subpages' => TRUE,
			
            'order' => 5,
            'pages' => array(

            	'resources' => array(
					'label' => 'Resources',
					'module' => 'admin',
					'controller' => 'resources',
					'permission' => 'administer all',
					'pages' => array(),
				),
				'categories' => array(
					'label' => 'Resource Categories',
					'module' => 'admin',
					'controller' => 'categories',
					'permission' => 'administer all',
					'pages' => array(),
				),
				'types' => array(
					'label' => 'Resource Types',
					'module' => 'admin',
					'controller' => 'types',
					'permission' => 'administer all',
					'pages' => array(),
				),

            	/*
				'settings'	=> array(
					'label'	=> 'Settings',
					'module' => 'admin',
					'controller' => 'settings',
					'action' => 'index',
					'permission' => 'administer all',
				),
				*/
				
                'users' => array(
                    'label' => 'Users',
                    'module' => 'admin',
                    'controller' => 'users',
					'action' => 'index',
					'permission' => 'manage users',
				),

				'permissions' => array(
					'label' => 'Permissions',
					'module' => 'admin',
					'controller' => 'permissions',
					'permission' => 'manage permissions',
					'pages' => array(),
				),

				'blocks' => array(
					'label' => 'Blocks',
					'module' => 'admin',
					'controller' => 'blocks',
					'action' => 'index',
					'permission' => 'administer all',
					'pages' => array(
						'blocks_edit' => array(
							'module' => 'admin',
							'controller' => 'blocks',
							'action' => 'edit',
						),
						'blocks_preview' => array(
							'module' => 'admin',
							'controller' => 'blocks',
							'action' => 'preview',
						),
					),
				),
            ),
        ),
    ),
);