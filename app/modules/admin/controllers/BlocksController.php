<?php
use \Entity\Block;

class Admin_BlocksController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer all');
    }
    
    public function indexAction()
    {
		$all_blocks = Block::fetchAll();
		$this->view->all_blocks = $all_blocks;
    }
	
	public function editAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->block->form);
		
		if ($this->hasParam('id'))
		{
			$record = Block::find((int)$this->getParam('id'));
			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Block))
				$record = new Block;
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Changes saved.');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
        }

        $this->view->tinymce();
        $this->view->headTitle('Add/Edit Content Block');
        $this->renderForm($form);
	}
	
	public function previewAction()
	{
		$record = Block::find((int)$this->getParam('id'));
		$this->view->block = $record;
	}
	
	public function deleteAction()
	{
        $record = Block::find((int)$this->getParam('id'));
		if ($record instanceof Block)
			$record->delete();
			
		$this->alert('Record deleted.', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
        return;
	}
}