<?php
use \Entity\User;
use \Entity\Role;

class Admin_UsersController extends \DF\Controller\Action
{
    public function permissions()
    {
		return \DF\Acl::getInstance()->isAllowed('administer all');
    }

    public function indexAction()
    {
        $user_query = $this->em->createQueryBuilder()
            ->select('u, r')
            ->from('Entity\User', 'u')
            ->leftJoin('u.roles', 'r');

        if ($this->_hasParam('q'))
        {
            $q = $this->_getParam('q');
            $this->view->q = $q;

            $user_query->where('u.email LIKE :q OR u.firstname LIKE :q OR u.lastname LIKE :q')->setParameter('q', '%'.$q.'%');
        }
        
        $this->view->pager = new \DF\Paginator\Doctrine($user_query, $this->_getParam('page', 1));
    }

    /*
    public function addAction()
    {
		$form = new \DF\Form($this->current_module_config->forms->user_new->form);
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
			$data = $form->getValues();
			$uins_raw = explode("\n", $data['uin']);
			
			foreach((array)$uins_raw as $uin)
			{
				$uin = trim($uin);
                
                if (strlen($uin) == 9)
                {
                    $user = User::getOrCreate($uin);
                    $user->assignRoles($data['roles']);
                    $user->save();
                    
                    $this->alert('User <a href="'.\DF\Url::route(array('module' => 'admin', 'controller' => 'users', 'action' => 'edit', 'id' => $user->id)).'" title="Edit User">'.$user->lastname.', '.$user->firstname.'</a> successfully updated/added.');
                }
			}
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
        }
		
        $this->view->form = $form;
    }
    */
    
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		$form = new \DF\Form($this->current_module_config->forms->user_edit->form);
		
		if ($user instanceof User)
			$form->setDefaults($user->toArray(TRUE, TRUE));
        else
            throw new \DF\Exception\DisplayOnly('User not found!');
        
		if( !empty($_POST) && $form->isValid($_POST) )
		{
			$data = $form->getValues();

			$user->fromArray($data);
			$user->save();
			
			$this->alert('<b>User updated!</b>', 'green');
			
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
		}

        $this->view->headTitle('Edit User');
        $this->renderForm($form);
    }
    
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		if ($user instanceof User)
            $user->delete();
        
        $this->alert('<b>User removed.</b>');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
}