<?php
/**
 * Personal profile form
 */
 
use \Entity\Settings;
 
$class_years = array();
for($i = date('Y') - 2; $i < date('Y') + 6; $i++)
{
	$class_years[$i] = $i;
}

$semesters = array();
for($i = 1; $i <= 10; $i++)
{
	$semesters[$i] = $i;
}

$profile_form = array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		
		'groups'		=> array(
			
			'personal'		=> array(
				'legend'		=> 'Personal Information',
				'elements'		=> array(
				
					'firstname'		=> array('text', array(
						'label' => 'First Name',
						'required' => true,
						'class' => 'half-width',
					)),
					'lastname'		=> array('text', array(
						'label' => 'Last Name',
						'required' => true,
						'class' => 'half-width',
					)),
					'mi'		=> array('text', array(
						'label' => 'Middle Initial',
					)),
					'sex'		=> array('radio', array(
						'label' => 'Sex',
						'multiOptions' => array('M' => 'Male', 'F' => 'Female'),
					)),
					'class'		=> array('select', array(
						'label' => 'Class Year',
						'multiOptions' => $class_years,
					)),
					'cphone'	=> array('text', array(
						'label' => 'Cell Phone (xxx-xxx-xxxx)',
					)),
					'lphone'	=> array('text', array(
						'label' => 'Local Home Phone (xxx-xxx-xxxx)',
					)),
					'lstreet'	=> array('text', array(
						'label'	=> 'Local Address',
						'class' => 'full-width',
					)),
					'lcity'		=> array('text', array(
						'label'	=> 'Local Address City',
					)),
					'lzip'		=> array('text', array(
						'label' => 'Local Address Zip',
					)),
					'email'		=> array('text', array(
						'label'	=> 'E-mail Address',
						'validators' => array('EmailAddress'),
					)),
					'dl'		=> array('text', array(
						'label' => 'Driver\'s License Number (xxxxxxxx)',
					)),
					'dl_state'	=> array('text', array(
						'label' => 'State Issuing Driver\'s License (xx)',
					)),
					'dl_curr'	=> array('radio', array(
						'label'	=> 'Is your driver\'s license current?',
						'multiOptions' => array('Y' => 'Yes', 'N' => 'No'),
					)),
					'dob'		=> array('text', array(
						'label' => 'Date of Birth (mm/dd/yyyy)',
					)),
					'shirt_size' => array('radio', array(
						'label' => 'T-Shirt Size',
						'multiOptions' => array(
							'S'		=> 'Small',
							'M'		=> 'Medium',
							'L'		=> 'Large',
							'XL'	=> 'X-Large',
							'XXL'	=> '2X-Large',
							'XXXL'	=> '3X-Large',
						),
					)),
					
				),
			),
			
			'emergency'		=> array(
				'legend'		=> 'Emergency Contact Information',
				'elements'		=> array(
					
					'emer1_name' => array('text', array(
						'label'	=> 'Contact 1 Name',
					)),
					'emer1_phone' => array('text', array(
						'label' => 'Contact 1 Phone (xxx-xxx-xxxx)',
					)),
					'emer2_name' => array('text', array(
						'label'	=> 'Contact 2 Name',
					)),
					'emer2_phone' => array('text', array(
						'label' => 'Contact 2 Phone (xxx-xxx-xxxx)',
					)),
					
				),
			),
			
			'parent'		=> array(
				'legend'		=> 'Parent/Guardian Contact Information',
				'elements'		=> array(
				
					'pphone'	=> array('text', array(
						'label'		=> 'Phone (xxx-xxx-xxxx)',
					)),
					'pstreet'	=> array('text', array(
						'label'		=> 'Address Street',
						'class'		=> 'half-width',
					)),
					'pcity'		=> array('text', array(
						'label'		=> 'Address City'
					)),
					'pstate'	=> array('text', array(
						'label'		=> 'Address State (xx)',
					)),
					'pzip'		=> array('text', array(
						'label'		=> 'Address Zip',
					)),
				
				),
			),
			
			'misc'		=> array(
				'legend'		=> 'Miscellaneous',
				'elements'		=> array(
					
					'medins'	=> array('radio', array(
						'label'		=> 'Do you have medical insurance?',
						'multiOptions' => array('Y' => 'Yes', 'N' => 'No'),
					)),
					'allergies' => array('text', array(
						'label'		=> 'Allergies to Medication',
						'class'		=> 'full-width',
					)),
					'semesters' => array('select', array(
						'label'		=> 'Semesters in CARPOOL (Including Current)',
						'multiOptions' => $semesters,
					)),
					'major'		=> array('text', array(
						'label'		=> 'College Major',
					)),
				
				),
			),
			
			'sms'		=> array(
				'legend'		=> 'Text Messaging Settings',
				'elements'		=> array(
				
					'sms_description' => array('markup', array(
						'label'			=> 'About Text Messaging',
						'markup'		=> '
							<p>In order to effectively notify members of CARPOOL in the case of important updates, reminders, or emergencies, the organization now supports the use of text messaging for easy delivery of information. By providing your cell phone number (or another number that can receive SMS messages, such as Google Voice) below, you will sign up for updates relating to events you attend, team activities, and other notifications.</p>
							
							<p><b>This service is completely optional.</b> Before signing up, you should ensure that your phone\'s plan includes text messaging, and that you are not close to your current quota. You can unsubscribe from this service at any time by removing your text messaging number from your profile.</p>
						',
					)),
				
					'sms_number' => array('text', array(
						'label'		=> 'Phone Number to Receive Text Messages (xxxxxxxxxx)',
						'description' => 'Avoid using landlines that cannot receive SMS messages, and do not include dashes or parenthesis.',
					)),
					'sms_initials' => array('text', array(
						'label'		=> 'Your Initials',
						'description' => 'By entering your initials, you agree to receive text messages at the number above. Applicable rates apply.',
					)),
				
				),
			),
		),
	),
);

// Show/hide the Personal Info Sheet (PIS) form components based on settings.
$show_pis_form = Settings::getSetting('cp_show_pis_form', 0);

if ($show_pis_form)
{
    $graduation_dates = array();
    $seasons = array('Spring', 'Summer', 'Fall');
    for($i = date('Y'); $i <= date('Y')+5; $i++)
    {
        foreach($seasons as $season)
        {
            $graduation_dates[$season.' '.$i] = $season.' '.$i;
        }
    }
    
    $pis_form = array(
        'legend'    => 'Personal Information Sheet Questions',
        'elements'  => array(
            
            'pis_graduation' => array('select', array(
                'label' => 'Graduation Date',
                'multiOptions' => $graduation_dates,
            )),
            
            'pis_returning' => array('radio', array(
                'label' => 'Do you plan on returning to CARPOOL next semester?',
                'multiOptions' => array(
                    'yes'       => 'Yes - Returning',
                    'yes_inactive' => 'Yes - Going Inactive',
                    'no'        => 'No - Not Returning',
                    'no_graduating' => 'No - Graduating',
                ),
            )),
            
            'pis_nightsandpoints' => array('radio', array(
                'label' => 'Will you make nights & points?',
                'multiOptions' => array(
                    'yes'       => 'Yes',
                    'no_appeal' => 'No - Will Appeal',
                    'no'        => 'No - Will Not Appeal',
                ),
            )),
            
            'pis_leavingreason' => array('textarea', array(
                'label' => 'If you are leaving, why?',
                'class' => 'full-width half-height',
            )),
            
        ),
    );
    
    $profile_form['form']['groups']['pis'] = $pis_form;
}


$profile_form['form']['groups']['submit'] = array(
    'elements'		=> array(
        'submit'		=> array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Save Changes',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);

return $profile_form;