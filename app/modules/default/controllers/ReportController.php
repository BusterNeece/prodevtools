<?php
use \Entity\ResourceLog;
use \Entity\ResourceCategory;
use \Entity\Resource;
use \Entity\User;

class ReportController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('is logged in');
	}

    public function indexAction()
    {
        if ($this->_hasParam('user_id'))
        {
            $this->acl->checkPermission('administer all');
            $user_id = (int)$this->_getParam('user_id');
            $user = User::find($user_id);
        }
        else
        {
            $user = $this->auth->getLoggedInUser();
        }

        $this->view->user = $user;

    	$time_per_category = array();
    	$time_total = 0;

    	if ($user->logs)
    	{
    		$logs = array();

    		foreach($user->logs as $log)
    		{
    			if ($log->seconds != 0)
    			{
    				$logs[] = $log;
	    			$time_total += $log->seconds;

	    			foreach($log->resource->categories as $category)
	    				$time_per_category[$category['id']] += $log->seconds;
	    		}
    		}

    		$this->view->logs = $logs;
    	}

    	$this->view->categories = ResourceCategory::fetchArray('name');
    	$this->view->time_per_category = $time_per_category;

    	$this->view->time_total = $time_total;
    }
}