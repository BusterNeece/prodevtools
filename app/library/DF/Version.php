<?php
/**
 * DF Core Framework Version
 **/

namespace DF;

define('DF_CORE_VERSION', 'DF_Z1_D2_2014B');

class Version
{
    public static function getVersion()
    {
        return DF_CORE_VERSION;
    }
}