<?php
namespace DF\View\Helper;

class FormMultiCheckbox extends \Zend_View_Helper_FormMultiCheckbox
{
    public function formMultiCheckbox($name, $value = null, $attribs = null, $options = null, $listsep = "<br />\n")
    {
        $raw = parent::formRadio($name, $value, $attribs, $options);

        $group_class = ($attribs['inline']) ? 'inline' : 'block';
        return '<div class="checkbox-group '.$group_class.'">'.$raw.'</div>';
    }
}
