<?php
namespace Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="resource_type")
 * @Entity
 */
class ResourceType extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    public static function fetchSelect()
    {
    	$items = array();
    	$items_raw = self::fetchArray('name');

    	foreach((array)$items_raw as $item)
    		$items[$item['id']] = $item['name'];

    	return $items;
    }
}