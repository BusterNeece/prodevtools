<?php
/**
 * Configuration for Third-Party APIs.
 */

return array(

    // Mandrill SMTP service.
    'smtp' => array(
        'server'        => 'smtp.mandrillapp.com',
        'port'          => '587',
        'auth'          => 'login',
        'username'      => '',
        'password'      => '',
    ),

    // ReCAPTCHA Service keys.
    'recaptcha' => array(
        'public_key' => '',
        'private_key' => '',
    ),

);