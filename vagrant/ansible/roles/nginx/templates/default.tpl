server {
    listen 80;

    server_name symfony;
    root        {{ doc_root }};

    sendfile off;

    error_log   /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log;

    location / {
        index       index.php;
        try_files   $uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite     ^(.*)$ /index.php/$1 last;
    }

    location ~ ^/(index)\.php(/|$) {
        fastcgi_pass            unix:/var/run/php5-fpm.sock;
        fastcgi_buffer_size     16k;
        fastcgi_buffers         4 16k;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param           HTTPS           off;
    }
}
